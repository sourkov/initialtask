import java.util.Scanner;

public class TaskCh01N017 {
    public static double userInput(){
        Scanner sc = new Scanner(System.in);
        boolean run = true;
        Double i = null;
        while (run) {
            System.out.print("Insert argument: ");
            String inputText = sc.nextLine();
            if (inputText.equals("exit"))
                System.exit(-1);
            try {
                i = Double.parseDouble(inputText);
                run = false;
            }catch (NumberFormatException e){
                System.out.println("Argument is not a real number! Try again!" + e.getLocalizedMessage());
            }
        }
        return i;
    }
 
    public static void main(String[] args) {
		System.out.println("This program calculates some mathematical expressions. To run this program you should insert arguments x, a, b, c. Arguments should be a real numbers. If you don't want to do calculation, please, type 'exit'");
		double x = userInput();
		System.out.println ("Got it! x value is " + x);
        double a = userInput();
		System.out.println ("Got it! a value is " + a);
        double b = userInput();
		System.out.println ("Got it! b value is " + b);
		double c = userInput();
		System.out.println ("Got it! c value is " + c + "\n \n");
		
		double paragraphO = Math.sqrt(1 - Math.pow(Math.cos(x),2)); //we can use Pythagorean trigonometric identity 1-(cos(x))^2 = (sinx)^2.Sqrt from (sinx)^2 = sinx
		System.out.println("Paragraph O calculation = " + paragraphO);
		//let's check our hypothesis
		double checkParagraphO = Math.sin(x);
		System.out.println("checkParagraph O calculation = " + checkParagraphO); 
		double paragraphP = 1/Math.sqrt(a * Math.pow(x,2) + b*x + c);
		System.out.println("paragraph P calculation = " + paragraphP);
		double paragraphR = Math.sqrt(x+1) + Math.sqrt(x-1)/2 * Math.sqrt(x);
		System.out.println("paragraph R calculation = " + paragraphR);
		double paragraphS = Math.abs(x) + Math.abs(x+1);
		System.out.println("paragraph S calculation = " + paragraphS);
    }
}