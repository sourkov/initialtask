import java.util.Scanner;
class TaskCh01N003 {
	public static void main(String[] args){
		System.out.print ("Please insert integer number from -2 147 483 648 to 2 147 483 648: ");
		Scanner scan = new Scanner(System.in);
		if (scan.hasNextInt()){
			int number = scan.nextInt();
			System.out.println("Your number is: " + number);
		} else {
			System.out.println("You've inserted not an integer number");
		}
	}
}